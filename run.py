#!/usr/bin/env python3

"""
Runs coala in a container to return file dicts and results.
"""
from copy import copy
import json
import os
import sys
from subprocess import run
from tempfile import mkdtemp
from shutil import rmtree

from coalib import get_version
from coalib.coala_main import run_coala
from coalib.output.JSONEncoder import create_json_encoder
from coalib.output.printers.ListLogPrinter import ListLogPrinter
from coala_utils.ContextManagers import suppress_stdout, replace_stderr


def runit(project_dir):
    """
    Runs coala!
    :param:
        Directory to run coala on.
    :return:
        A dictionary that holds a list of all results for all section names as
        well as another dictionary associating the same section names with file
        dicts along with coala version.
    """
    log_printer = ListLogPrinter()

    oldargv = copy(sys.argv)
    sys.argv.clear()
    sys.argv.extend(['coala', '-c='+project_dir])
    with suppress_stdout():
        results, _, file_dicts = run_coala(
            log_printer=log_printer, autoapply=False)
    version = get_version()
    sys.argv.clear()
    sys.argv.extend(oldargv)

    return results, file_dicts, version


def main():
    """
    Starts project code analysis!
    """
    sha = sys.argv[1]
    repo_url = sys.argv[2]
    fetch_ref = sys.argv[3]
    try:
        project_dir = mkdtemp()

        run(['git', 'clone', '--depth=100', repo_url, project_dir],
            cwd=project_dir)
        run(['git', 'fetch', repo_url, fetch_ref], cwd=project_dir)
        run(['git', 'checkout', sha], cwd=project_dir)

        if len(sys.argv) > 4:
            coafile = os.path.join(project_dir, sys.argv[4])
        else:
            coafile = project_dir

        results, file_dicts, version = runit(coafile)
        JSONEncoder = create_json_encoder()

        print(json.dumps({
            'version': version,
            'results': results,
            'file_dicts': file_dicts
        }, cls=JSONEncoder))
    finally:
        rmtree(project_dir)


if __name__ == '__main__':
    main()
